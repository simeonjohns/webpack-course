import React from 'react';
import { render } from "react-dom";

import * as $ from 'jquery';
import Post from '@models/Post';
import '@/styles/index.css';
import '@/styles/less.less';
import '@/styles/scss.scss';
import logo from '@/assets/webpack-logo';
import './babel';

const post = new Post('The first post', logo);

$('pre').addClass('code').html(post.toString());


const App = () => (
  <div className="container">
    <h1>Webpack Course</h1>

    <hr />

    <div className="logo"></div>

    <hr />

    <pre />

    <hr />

    <div className="box">
      <h2>Less</h2>
    </div>

    <hr />

    <div className="card">
      <h2>SCSS</h2>
    </div>
  </div>
);


render(<App />, document.getElementById('app'));

