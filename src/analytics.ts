import * as $ from 'jquery';


function createAnalytics(): object {
  let counter = 0;
  let isDistroted = false;

  const handleGlobalClick = () => $('pre').html(counter++);

  $(document).on('click', handleGlobalClick);

  return {
    destroy() {
      $(document).off('click', handleGlobalClick);
      isDistroted = true;
    },

    getClick() {
      if (isDistroted) {
        $('pre').html(`Analytics is destroyed! Total clicks: ${counter}`);
      }
      $('pre').html(counter);
    },
  }
}

window['analytics'] = createAnalytics();